import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import ToggleButton from '@mui/material/ToggleButton';
import red from '../../Image/red.png'
import red1 from '../../Image/red1.png'
import red2 from '../../Image/red2.png'
import yellow from '../../Image/yellow.png'
import yellow1 from '../../Image/yellow1.png'
import yellow2 from '../../Image/yellow2.png'
import blue from '../../Image/blue.png'
import blue1 from '../../Image/blue1.png'
import aucunneImage from '../../Image/aucune image.png'
import {IconButton, ToggleButtonGroup} from "@mui/material";
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';




const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const theme = createTheme();
export default function IAPage () {


    const [image, setImage] = React.useState(aucunneImage);
    const [imageUrl, setImageUrl] = React.useState(aucunneImage);
    const [reponse, setReponse] = React.useState();


    function onImageChange(event){
        setImage(event.target.files[0]);
        setImageUrl(URL.createObjectURL(event.target.files[0]));
    }


    const handleSubmit = (event) => {
        event.preventDefault();
        var formData = new FormData();
        formData.append('image',image)


        fetch("/api/getImage/",{
        method: 'POST',
        body: formData
        })
            .then(res => res.json())
            .then(
                (result) => {
                    setReponse(result)
                },
            )
    };


    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />
            <AppBar position="relative">
                <Toolbar>
                    <Typography variant="h6" color="inherit" noWrap>
                        IA Cancer
                    </Typography>
                </Toolbar>
            </AppBar>
            <main>
                {/* Hero unit */}
                <Box
                    sx={{
                        bgcolor: 'background.paper',
                        pt: 8,
                        pb: 6,
                    }}
                >
                    <Container sx={{ py: 8 }}>
                        <Typography
                            component="h1"
                            variant="h2"
                            align="center"
                            color="text.primary"
                            gutterBottom
                        >
                            Detection de type de mélanome
                        </Typography>
                        <Typography variant="h5" align="center" color="text.secondary" paragraph>
                            Bienvenue sur notre interface dédiée à la détection de mélanomes. Cet outil de recherche utilise des modèles de réseaux de neurones convolutionnels (CNN) pour analyser des images médicales. En fonction de l'image analysée, l'interface propose une prédiction sur le type spécifique de mélanome identifié. Il s'agit d'un outil de recherche avancée, conçu pour aider à la détection précoce et à la compréhension des mélanomes. Cependant, il est essentiel de rappeler que les résultats fournis par notre interface doivent être confirmés par un spécialiste médical pour un diagnostic précis et fiable.
                        </Typography>
                    </Container>
                </Box>
                <Container sx={{ py: 8 }}>
                    {/* End hero unit */}



                    <Grid container spacing={3}>

                        <Grid item xs={12} sm={2} md={3}>
                        <Card sx={{ maxWidth: 280 }}>
                            <CardMedia
                                sx={{ height: 160 }}
                                image={yellow}
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    Efficient B2
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    Ce modèle est un bon équilibre entre précision et vitesse. Il est plus rapide et utilise moins de ressources que certains modèles plus gros, tout en restant très précis.
                                </Typography>
                            </CardContent>
                        </Card>
                        </Grid>

                        <Grid item xs={12} sm={2} md={3}>
                            <Card sx={{ maxWidth: 280 }}>
                                <CardMedia
                                    sx={{ height: 160 }}
                                    image={yellow1}
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="div">
                                        Efficient B4
                                    </Typography>
                                    <Typography variant="body2" color="text.secondary">
                                        Un cran au-dessus du B2, ce modèle est légèrement plus lent et utilise un peu plus de ressources, mais il est généralement plus précis, particulièrement pour les images plus complexes.
                                    </Typography>
                                </CardContent>

                            </Card>
                        </Grid>

                        <Grid item xs={12} sm={2} md={3}>
                            <Card sx={{ maxWidth: 280 }}>
                                <CardMedia
                                    sx={{ height: 160 }}
                                    image={yellow2}
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="div">
                                        Efficient B7
                                    </Typography>
                                    <Typography variant="body2" color="text.secondary">
                                        Le plus grand modèle de la famille EfficientNet. Il est le plus précis, mais il est aussi le plus lent et consomme le plus de ressources. Idéal si la précision est la priorité et que le temps et les ressources ne sont pas un problème.
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={2} md={3}>
                            <Card sx={{ maxWidth: 280 }}>
                                <CardMedia
                                    sx={{ height: 160 }}
                                    image={red}
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="div">
                                        ResNet 50
                                    </Typography>
                                    <Typography variant="body2" color="text.secondary">
                                        Un modèle solide avec un bon équilibre entre vitesse et précision. Il est particulièrement efficace pour identifier les caractéristiques de niveau intermédiaire dans les images.
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={2} md={3}>
                            <Card sx={{ maxWidth: 280 }}>
                                <CardMedia
                                    sx={{ height: 160 }}
                                    image={red1}
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="div">
                                        ResNet RS200
                                    </Typography>
                                    <Typography variant="body2" color="text.secondary">
                                        Une version plus grande et plus précise de ResNet, mais elle est aussi beaucoup plus lente et nécessite plus de ressources. Elle peut être utile pour des images très détaillées.

                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={2} md={3}>
                            <Card sx={{ maxWidth: 280 }}>
                                <CardMedia
                                    sx={{ height: 160 }}
                                    image={red2}
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="div">
                                        ResNet V2
                                    </Typography>
                                    <Typography variant="body2" color="text.secondary">
                                        Une amélioration du ResNet original, avec une meilleure précision sans être beaucoup plus lent. Il est un bon choix si vous voulez une petite amélioration par rapport à ResNet 50.
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={2} md={3}>
                            <Card sx={{ maxWidth: 280 }}>
                                <CardMedia
                                    sx={{ height: 160 }}
                                    image={blue}
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="div">
                                        VGG 16
                                    </Typography>
                                    <Typography variant="body2" color="text.secondary">
                                        Un modèle plus ancien mais toujours fiable. Il est moins précis que certains des autres modèles mentionnés, mais il est plus simple et souvent plus rapide à exécuter.
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={2} md={3}>
                            <Card sx={{ maxWidth: 280 }}>
                                <CardMedia
                                    sx={{ height: 160 }}
                                    image={blue1}
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="div">
                                        VGG 19
                                    </Typography>
                                    <Typography variant="body2" color="text.secondary">
                                        Il s'agit d'une version plus grande de VGG 16, avec plus de couches pour une meilleure précision, mais il est aussi plus lent et nécessite plus de ressources.
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>

                    </Grid>
                    </Container>
                    <Container sx={{ py: 8 }} maxWidth="md">


                    <Container sx={{ textAlign: 'center', py: 8 }}>
                        <IconButton color="primary" aria-label="upload picture" component="label" sx={{marginLeft: "10px"}}>
                            <input hidden accept="image/*" type="file" onChange={onImageChange}/>
                            <AddPhotoAlternateIcon />
                        </IconButton>
                        <Card sx={{ marginTop: '15px' }}>
                            <img src={imageUrl} sx={{ align: 'center'}}/>
                        </Card>
                    </Container>

                </Container>
                <form onSubmit={handleSubmit}>
                <Container sx={{ textAlign: 'center' }}>
                    <Button type="submit" variant="contained">Traiter l'image</Button>
                    <Typography>
                        {reponse}
                    </Typography>
                </Container>
                </form>


            </main>
            {/* Footer */}
            <Box sx={{ bgcolor: 'background.paper', p: 6 }} component="footer">
                <Typography variant="h6" align="center" gutterBottom>
                    A savoir
                </Typography>
                <Typography
                    variant="subtitle1"
                    align="center"
                    color="text.secondary"
                    component="p"
                >
                    Veuillez noter que ces travaux sont à titre de recherche et ne remplacent pas l'expertise d'un professionnel de la santé. Pour un diagnostic plus précis et fiable, il est essentiel de consulter un spécialiste médical.
                </Typography>
            </Box>
            {/* End footer */}
        </ThemeProvider>
    );
}
