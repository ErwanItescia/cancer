import './App.css';
import IAPage from "./Modules/IAPage/IAPage";
import {Route, Routes} from 'react-router-dom'

function App() {
  return (
      <div id="page">
        <Routes>
          <Route exact path='/' element={< IAPage />}></Route>
        </Routes>
      </div>

  );
}

export default App;
