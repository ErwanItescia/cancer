FRONT
    "@emotion/react": "^11.10.6",
    "@emotion/styled": "^11.10.6",
    "@mui/icons-material": "^5.11.11",
    "@mui/material": "^5.11.12",
    "@mui/styled-engine-sc": "^5.11.11",
    "@testing-library/jest-dom": "^5.16.5",
    "@testing-library/react": "^13.4.0",
    "@testing-library/user-event": "^13.5.0",
    "react": "^18.2.0",
    "react-dom": "^18.2.0",
    "react-router-dom": "^6.8.2",
    "react-scripts": "5.0.1",
    "styled-components": "^5.3.8",
    "web-vitals": "^2.1.4"
BACK
	Flask	2.2.3
	Jinja2 3.1.2
	MarkupSafe 2.1.2
	Werkzeug 2.2.3
	click	8.1.3
	colorama 0.4.6
	itsdangerous 2.1.2
	pip 21.3.1
	setuptools 60.2.0
	wheel 0.37.1
